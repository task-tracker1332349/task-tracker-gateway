package ru.muslimov.tasktrackergateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskTrackerGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaskTrackerGatewayApplication.class, args);
    }

}
